package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.projects.configuration.ProjectConfiguration;
import com.softtek.academy.projects.model.JornadaMensual;
import com.softtek.academy.projects.model.Registro;
import com.softtek.academy.projects.repository.JornadaMensualJpaRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProjectConfiguration.class })
@WebAppConfiguration
public class misTests {
	@Autowired
	private JornadaMensualJpaRepository jornadaMensual;
	
	@Test
	public void testOpenExcel() throws EncryptedDocumentException, InvalidFormatException, IOException{
		FileInputStream file = new FileInputStream(new File("RegistrosMomentum.xlsx"));
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        
        assertNotNull(file);
        assertNotNull(workbook);
        
	}
	
	@Test
	public void testJornadaMensual() {
		assertNotNull(jornadaMensual.findByMonth(5));
		assertNotNull(jornadaMensual.findByUsernameAndMonth("JABS1", 3));
	}
	
	@Test
	public void testDates() {
		Date date1 = null;
		Date date2 = null;
		
		try{
			date1 = StringToDate("2019-12-12 00:00:00");
			date2 = StringToDate("2019-12-12 03:00:00");
			
			//Checar que las funciones que comparan los días funcionen correctamente
			assertNotNull(date1);
			assertNotNull(date2);
			assertTrue(validateMismoDia(date1, date2));
			assertTrue(checkDate(date1, date2));
			
			date1 = StringToDate("2019-12-15 08:00:00");
			date2 = StringToDate("2019-12-15 15:00:00");
			System.out.println(getHorasPorRegistro(date1, date2));
			assertEquals(new Double(getHorasPorRegistro(date1, date2)), new Double(7));
		} catch (Exception e) {
			e.printStackTrace();
			date1 = null;
			date2 = null;
		}
		
	}
	
	
	private boolean checkDate(Date dateInParsed, Date dateOutParsed) {
		//Checamos que el día coincida, al igual que el año
		boolean sameDay = validateMismoDia(dateInParsed, dateOutParsed);
		if(sameDay == false) {return false;}
		
		//Checamos que al menos haya trabajado 1 hora para evitar 2 registros de entrada o 2 de salida.		
		double hours = getHorasPorRegistro(dateInParsed, dateOutParsed);
		if(hours < 1) {return false;}
		return true;
	}
	
	private boolean validateMismoDia(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		//Checamos que el día coincida, al igual que el año
		return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) 
				&& cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
	}
	
	private double getHorasPorRegistro(Date date1, Date date2) {
		DateTime dateTimeIn = new DateTime(date1);
		DateTime dateTimeOut = new DateTime(date2);
		Period p = new Period (dateTimeIn, dateTimeOut);
		
		//System.out.println("Horas -> " + p.getHours() + "\tMinutos -> " + p.getMinutes() + "\tSegundos = " + p.getSeconds());
		//Hacer esto parsea los datos para almacenarlos en forma de un entero.
		return (double)p.getHours() + ((double)p.getMinutes() / 60) + ((double)p.getSeconds()/ 3600);
		
	}
	
	public static Registro setRegistro(Row row) throws Exception {
		String nameIn = row.getCell(2).getStringCellValue();
        String dateIn = row.getCell(4).getStringCellValue();
        Date dateInParsed = StringToDate(dateIn);
		return new Registro(nameIn, dateInParsed);
	}
	
	public static Date StringToDate(String dateIn) throws Exception{
		//Partimos la fecha base en 2, la fecha y la hora
        String [] baseDateInParts = dateIn.split(" ");
        //Partimos la fecha en sus componentes; día, mes y año
        String [] dateInPartDay = baseDateInParts[0].split("-");
		//Juntamos los datos y retornamos como date
        String s = dateInPartDay[2]+"/"+ dateInPartDay[1]+"/"+ dateInPartDay[0]+" "+ baseDateInParts[1];
        System.out.println(s);
	    return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(s);  
	}
	
}
