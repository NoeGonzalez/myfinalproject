<!DOCTYPE html>
    <%  String contextPath = request.getContextPath();
        String title = "Student List"; 	
        //Endpoint 1
        String Ep1IS = "";
        int Ep1Mes = 1;
        
        %>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
	
      <!-- ------------------------Apartado de los m�todos GET -------------------------------- -->

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Endpoints GET
        </a>
        <div class="dropdown-menu text-center">
            <form class="form my-2 my-lg-0 m-3" method="GET" action="<%=contextPath%>/api/v1/users/<%=Ep1IS%>/<%=Ep1Mes%>">
              
              Endpoint 1: Horas por mes de un trabajador
		      <input type="text" class="form-control" placeholder="IS"  value="<%=Ep1IS%>">
		      <input type="number" min="1" max="12" class="form-control mr-sm-2" placeholder="Mes" value="<%=Ep1Mes%>">
		      <button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> Buscar </button>
		      <div>Mes -> <%=Ep1Mes%></div>
		      <div>IS -> <%=Ep1IS%></div>
		      
		    </form>
		    <br>
            <form class="form-inline my-2 my-lg-0 m-3">
            <div>
              Endpoint 3: Registros de usuarios por mes
              </div>
		      <input class="form-control mr-sm-2" type="search" placeholder="Search">
		      <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Buscar</button>
		    </form>
    	</div>
      </li>
      
      <!-- ------------------------- Apartado de los m�todos POST --------------------------------- -->

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Endpoints POST
        </a>
        <div class="dropdown-menu text-center" aria-labelledby="navbarDropdown">
            <form class="form-inline my-2 my-lg-0 m-3" method="POST" action="/api/v1/lel">
              <div>
              Endpoint 2: Horas por mes de un trabajador
              </div>
		      <input class="form-control mr-sm-2" type="search" placeholder="IS">
		      <input type="date" class="form-control mr-sm-2" type="search" placeholder="Fecha Inicial">
		      <input type="date" class="form-control mr-sm-2" type="search" placeholder="Fecha Final">
		      <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">
		       	<!-- onClick="javascript:myFunction()">  -->
		       		Buscar
		       </button>
		    </form>
		    <br>
            <form class="form-inline my-2 my-lg-0 m-3" method="POST">
            <div>
              Endpoint 4: Registros de usuarios por mes
              </div>
		      <input class="form-control mr-sm-2" type="search" placeholder="Search">
		      <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Buscar</button>
		    </form>
    	</div>
      </li>
      
      
    <hr>   
    <li class="nav-item">
        <a class="nav-link" href="<%=contextPath%>/documents/update">Actualizar Base de Datos</a>
    </li>
	<hr>
	
    </ul>
    
    
  </div>
</nav>
</body>

	
	<script>
		function myFunction() {
			window.location.href = "http://www.w3schools.com";
			return false;
		}
	</script>
</html>
