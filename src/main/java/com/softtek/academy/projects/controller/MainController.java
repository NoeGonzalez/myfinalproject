package com.softtek.academy.projects.controller;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.projects.model.JornadaMensual;
import com.softtek.academy.projects.model.Registro;
import com.softtek.academy.projects.model.RegistroInOut;
import com.softtek.academy.projects.service.JornadaMensualService;

@RestController
@RequestMapping("api/v1")
public class MainController {
	
	@Autowired
	JornadaMensualService jornadaService;
	
	@RequestMapping(value="users/{IS}/{mes}", method = RequestMethod.GET)
	public JornadaMensual endpoint1(@PathVariable String IS, @PathVariable int mes) {
		JornadaMensual test = jornadaService.findByUsernameAndMonth(IS, mes);
		if(test.equals(null)) {
			test = new JornadaMensual(0,"Lo sentimos, no se recuperaron datos...", 0, 0);
		}
		return test;
	}
	
	@RequestMapping(value="period", method = RequestMethod.POST)
	public List<RegistroInOut> endpoint4(@RequestBody String values) {
		System.out.println(values);
		
		String [] splitValues = values.split("&");
		String [] firstDateSplit = splitValues[0].split("=");
		String [] lastDateSplit = splitValues[1].split("=");
		
		//new java.util.Scanner(System.in).nextLine();
		
		//Dia, mes y año se encuentran en los valores 0, 1 y 2 de los siguientes 2 arreglos de strings
		String [] firstDateComponents = firstDateSplit[1].split("%2F");
		String [] lastDateComponents = lastDateSplit[1].split("%2F");
		
		String firstDate = firstDateComponents[2]+"-" +
						   firstDateComponents[1]+"-" +
						   firstDateComponents[0] + 
							" 00:00:00";
		String lastDate = lastDateComponents[2] + "-" +
						  lastDateComponents[1] + "-" +
						  lastDateComponents[0] +
						  " 23:59:59";

		
		//new java.util.Scanner(System.in).nextLine();
		
		List<RegistroInOut> misRegistros = new ArrayList<>();
		
		//variables que controlan la información que se maneja en los registros.
        Registro registro1 = null; //Registro que se compara con el registro2 para verificacion
        Registro registro2 = null;//Registro que se compara con el registro1 para verificacion
        Registro registroBackup = null; //Registro que reespalda la información del registro1 en cada iteracion
        double horasPorMes = 0; //Contador para verificar cuantas horas se realizan por mes
        int registrosMensualesId = 1; //Id usado para subir a la base de datos. No es de mucha importancia
        int mes = 0; //Tiene la info del mes que se subirá al registro
		
		try {
			Date requestFirstDate = StringToDate(firstDate);
			Date requestLastDate = StringToDate(lastDate);
			
			FileInputStream file = new FileInputStream(new File("RegistrosMomentum.xlsx"));
	        XSSFWorkbook workbook = new XSSFWorkbook(file);
	        XSSFSheet sheet = workbook.getSheetAt(0);
	        Iterator<Row> rowIterator = sheet.iterator();
	        rowIterator.next();
	        //Leemos los registros 
	        while(rowIterator.hasNext()) {
	        	//Entramos al registro actual
            	Row rowActual = rowIterator.next();
            	if(registro1 == null) {
            		//Si no existe un registro ya guardado, guardamos el actual y pasamos al siguiente
            		registro1 = setRegistro(rowActual);
            		rowActual = rowIterator.next();
            		//mes = registro2.getMonth();
            	}
            	//Guardamos el registro actual para comparar
            	registro2 = setRegistro(rowActual);
            	//Imprimimos los 2 registros a comparar
            	System.out.println(registro1.toString());
            	System.out.println(registro2.toString());
            	
            	//Si los registros no coinciden en los nombres, actualizamos el registro de base
            	//y dejamos que el ciclo while se encargue de actualizar el actual después.
            	if( !registro1.getName().equals(registro2.getName()) ) {
            		registro1 = setRegistro(rowActual);
            		registrosMensualesId++;
            		horasPorMes = 0;
            		mes = registro2.getMonth();
            	}else {
            		/* Si los nombres coinciden pero la fecha no, actualizamos el registro base */
            		if(!checkDate(registro1.getDate(), registro2.getDate())) {
            			registro1 = setRegistro(rowActual);
            			mes = registro2.getMonth();
            		}else {
            			//En este punto, el nombre y la fecha coinciden. Ahora corroboramos si trabajó al menos 1 hora
            			System.out.println(" Buena jornada de Trabajo, campeón!");
            			
            			if(entreFechas(requestFirstDate, requestLastDate, registro1.getDate())) {
            				misRegistros.add(new RegistroInOut(registrosMensualesId, registro1.getName(), registro1.getDate(), registro2.getDate()));
            				//new java.util.Scanner(System.in).nextLine();
            			}
            			
            			registrosMensualesId++;
            			
            			horasPorMes += getHorasPorRegistro(registro1.getDate(), registro2.getDate());
            			System.out.print("Horas acumuladas en el mes de "+ registro2.getMonth()+ " -> " 
            						+String.format("%.2f", horasPorMes));
            			//Hacemos un backup, por si el siguiente registro tiene un mes diferente
            			//y debemos recuperarlo
            			registroBackup = registro1;
            			registro1 = null; 
            			mes = registro2.getMonth();
            			
            		} //Fin del if para comparar fechas
            	} //Fin del if para comparar nombres
                System.out.println();
            }//Fin de ciclo while que recorre los datos del excel
            System.out.println("Fin de la lectura del archivo");
            file.close();
			
		} catch (Exception e) { e.printStackTrace(); }
		
		if(misRegistros.isEmpty()) {
			misRegistros.add(new RegistroInOut(0, "Lo sentimos, registros no encontrados...", new Date(), new Date()));
		}
		return misRegistros;
	}
	
	@RequestMapping(value="period/{mes}", method = RequestMethod.GET)
	public List<JornadaMensual> endpoint3(@PathVariable int mes) {
		List<JornadaMensual> test = jornadaService.findByMonth(mes);
		if(test.isEmpty()) {
			test.add(new JornadaMensual(0,"Lo sentimos, no se encontraron registros...", 0,0));
		}
		return test;
	}
	
	@RequestMapping(value="users", method=RequestMethod.POST)
	public List<RegistroInOut> endpoint2(@RequestBody String values) {
		System.out.println(values);
		
		//new java.util.Scanner(System.in).nextLine();
		
		String [] splitValues = values.split("&");
		
		String [] firstDateSplit = splitValues[1].split("=");
		String [] lastDateSplit = splitValues[2].split("=");
		String requestIS = splitValues[0].split("=")[1];
		
		//Dia, mes y año se encuentran en los valores 0, 1 y 2 de los siguientes 2 arreglos de strings
		String [] firstDateComponents = firstDateSplit[1].split("%2F");
		String [] lastDateComponents = lastDateSplit[1].split("%2F");
		
		String firstDate = firstDateComponents[2]+"-" +
						   firstDateComponents[1]+"-" +
						   firstDateComponents[0] + 
							" 00:00:00";
		String lastDate = lastDateComponents[2] + "-" +
						  lastDateComponents[1] + "-" +
						  lastDateComponents[0] +
						  " 23:59:59";
		
		List<RegistroInOut> misRegistros = new ArrayList<>();
		
		//variables que controlan la información que se maneja en los registros.
        Registro registro1 = null; //Registro que se compara con el registro2 para verificacion
        Registro registro2 = null;//Registro que se compara con el registro1 para verificacion
        Registro registroBackup = null; //Registro que reespalda la información del registro1 en cada iteracion
        double horasPorMes = 0; //Contador para verificar cuantas horas se realizan por mes
        int registrosMensualesId = 1; //Id usado para subir a la base de datos. No es de mucha importancia
        int mes = 0; //Tiene la info del mes que se subirá al registro
		
		try {
			Date requestFirstDate = StringToDate(firstDate);
			Date requestLastDate = StringToDate(lastDate);
			
			FileInputStream file = new FileInputStream(new File("RegistrosMomentum.xlsx"));
	        XSSFWorkbook workbook = new XSSFWorkbook(file);
	        XSSFSheet sheet = workbook.getSheetAt(0);
	        Iterator<Row> rowIterator = sheet.iterator();
	        rowIterator.next();
	        //Leemos los registros 
	        while(rowIterator.hasNext()) {
	        	//Entramos al registro actual
            	Row rowActual = rowIterator.next();
            	if(registro1 == null) {
            		//Si no existe un registro ya guardado, guardamos el actual y pasamos al siguiente
            		registro1 = setRegistro(rowActual);
            		rowActual = rowIterator.next();
            		//mes = registro2.getMonth();
            	}
            	//Guardamos el registro actual para comparar
            	registro2 = setRegistro(rowActual);
            	//Imprimimos los 2 registros a comparar
            	System.out.println(registro1.toString());
            	System.out.println(registro2.toString());
            	
            	//Si los registros no coinciden en los nombres, actualizamos el registro de base
            	//y dejamos que el ciclo while se encargue de actualizar el actual después.
            	if( !registro1.getName().equals(registro2.getName())) {
            		registro1 = setRegistro(rowActual);
            		registrosMensualesId++;
            		horasPorMes = 0;
            		mes = registro2.getMonth();
            	}else {
            		/* Si los nombres coinciden pero la fecha no, actualizamos el registro base */
            		if(!checkDate(registro1.getDate(), registro2.getDate())) {
            			registro1 = setRegistro(rowActual);
            			mes = registro2.getMonth();
            		}else {
            			//En este punto, el nombre y la fecha coinciden. Ahora corroboramos si trabajó al menos 1 hora
            			System.out.println(" Buena jornada de Trabajo, campeón!");
            			
            			if(entreFechas(requestFirstDate, requestLastDate, registro1.getDate()) 
            					&& compareStrings(registro1.getName(), requestIS) ) {
            				misRegistros.add(new RegistroInOut(
            						registrosMensualesId, 
            						registro1.getName(), 
            						registro1.getDate(), 
            						registro2.getDate()));
            			}
            			
            			registrosMensualesId++;
            			
            			horasPorMes += getHorasPorRegistro(registro1.getDate(), registro2.getDate());
            			//System.out.print("Horas acumuladas en el mes de "+ registro2.getMonth()+ " -> " +String.format("%.2f", horasPorMes));
            			//Hacemos un backup, por si el siguiente registro tiene un mes diferente
            			//y debemos recuperarlo
            			registroBackup = registro1;
            			registro1 = null; 
            			mes = registro2.getMonth();
            			
            		} //Fin del if para comparar fechas
            	} //Fin del if para comparar nombres
                System.out.println();
            }//Fin de ciclo while que recorre los datos del excel
            System.out.println("Fin de la lectura del archivo");
            file.close();
			
		} catch (Exception e) { e.printStackTrace(); }
		
		if(misRegistros.isEmpty()) {
			misRegistros.add(new RegistroInOut(0, "Lo sentimos, registros no encontrados...", new Date(), new Date()));
		}
		return misRegistros;
	}
	
	private boolean compareStrings(String s1, String s2) {
		System.out.println("s1 -> " + s1 + "\ts2 -> " + s2);
		boolean hello = s1.equals(s2);
		if(hello) {
			System.out.println("Son iguales!");
		} else System.out.println("No son iguales :c");
		return hello;
	}
	
	private boolean checkDate(Date dateInParsed, Date dateOutParsed) {
		//Checamos que el día coincida, al igual que el año
		boolean sameDay = validateMismoDia(dateInParsed, dateOutParsed);
		if(sameDay == false) {
			System.out.println("Días diferentes, cancelando ingreso...");
			return false;
		}
		
		//Checamos que al menos haya trabajado 1 hora para evitar 2 registros de entrada o 2 de salida.		
		double hours = getHorasPorRegistro(dateInParsed, dateOutParsed);
		if(hours < 1) {
			//System.out.println("Registro con doble dato de entrada o salida, cancelando ingreso...");
			return false;
		}
		return true;
	}
	
	private boolean validateMismoDia(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		//Checamos que el día coincida, al igual que el año
		return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) 
				&& cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
	}
	
	private boolean entreFechas(Date min, Date max, Date dateBetween) {
		System.out.println("Fechas -> " + min + " | " + max + " | " + dateBetween);
		boolean dato = dateBetween.after(min) && dateBetween.before(max);
		if(dato) {
			System.out.println("YES! DateBetween inside them");
		}
		return dato;
	}
	
	private double getHorasPorRegistro(Date date1, Date date2) {
		DateTime dateTimeIn = new DateTime(date1);
		DateTime dateTimeOut = new DateTime(date2);
		Period p = new Period (dateTimeIn, dateTimeOut);
		//System.out.println("Horas -> " + p.getHours() + "\tMinutos -> " + p.getMinutes() + "\tSegundos = " + p.getSeconds());
		//Hacer esto parsea los datos para almacenarlos en forma de un entero.
		return (double)p.getHours() + ((double)p.getMinutes() / 60) + ((double)p.getSeconds()/ 3600);
	}
	
	public static Registro setRegistro(Row row) throws Exception {
		String nameIn = row.getCell(2).getStringCellValue();
        String dateIn = row.getCell(4).getStringCellValue();
        Date dateInParsed = StringToDate(dateIn);
		return new Registro(nameIn, dateInParsed);
	}
	
	public static Date StringToDate(String dateIn) throws Exception{
		//Partimos la fecha base en 2, la fecha y la hora
        String [] baseDateInParts = dateIn.split(" ");
        //Partimos la fecha en sus componentes; día, mes y año
        String [] dateInPartDay = baseDateInParts[0].split("-");
		//Juntamos los datos y retornamos como date
        String s = dateInPartDay[2]+"/"+ dateInPartDay[1]+"/"+ dateInPartDay[0]+" "+ baseDateInParts[1];
        System.out.println(s);
	    return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(s);  
	}
}

