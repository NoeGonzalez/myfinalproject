package com.softtek.academy.projects.controller;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.projects.model.JornadaMensual;
import com.softtek.academy.projects.model.Registro;
import com.softtek.academy.projects.service.JornadaMensualService;

@RestController
@RequestMapping("documents")

public class ExcelController {
	
	@Autowired
	JornadaMensualService jornadaService;
	
	@RequestMapping(value="update", method = RequestMethod.GET)
	public void updateDBfromExcel(HttpServletResponse response) {
		 try{
			 //System.out.println("working directory -> "+ System.getProperty("user.dir"));
	            FileInputStream file = new FileInputStream(new File("RegistrosMomentum.xlsx"));
	            //Create Workbook instance holding reference to .xlsx file
	            XSSFWorkbook workbook = new XSSFWorkbook(file);
	            //Get first/desired sheet from the workbook
	            XSSFSheet sheet = workbook.getSheetAt(0);
	            //Iterate through each rows one by one
	            Iterator<Row> rowIterator = sheet.iterator();
	            
	            //variables que controlan la información que se maneja en los registros.
	            Registro registro1 = null; //Registro que se compara con el registro2 para verificacion
	            Registro registro2 = null;//Registro que se compara con el registro1 para verificacion
	            Registro registroBackup = null; //Registro que reespalda la información del registro1 en cada iteracion
	            double horasPorMes = 0; //Contador para verificar cuantas horas se realizan por mes
	            int numRegistros = 0; // Cuenta al final cuantos registros completos (entrada+salida) se realizaron
	            int registrosMensualesId = 1; //Id usado para subir a la base de datos. No es de mucha importancia
	            int mes = 0; //Tiene la info del mes que se subirá al registro
	            
	            rowIterator.next();
	            while (rowIterator.hasNext()) {
	            	//Entramos al registro actual
	            	Row rowActual = rowIterator.next();
	            	if(registro1 == null) {
	            		//Si no existe un registro ya guardado, guardamos el actual y pasamos al siguiente
	            		registro1 = setRegistro(rowActual);
	            		rowActual = rowIterator.next();
	            		//mes = registro2.getMonth();
	            	}
	            	//Guardamos el registro actual para comparar
	            	registro2 = setRegistro(rowActual);
	            	//Imprimimos los 2 registros a comparar
	            	System.out.println(registro1.toString());
	            	System.out.println(registro2.toString());
	            	
	            	//Si los registros no coinciden en los nombres, actualizamos el registro de base
	            	//y dejamos que el ciclo while se encargue de actualizar el actual después.
	            	if( !registro1.getName().equals(registro2.getName()) ) {
	            		System.out.println("Trabajadores Diferentes, subiendo registro del trabajador anterior...");
	            		//Guardamos el registro si se cambia de trabajador, porque entonces su lectura terminó
	            		jornadaService.save(new JornadaMensual(
	            						registrosMensualesId, 
	            						registro1.getName(), 
	            						registro1.getMonth(), 
	            						horasPorMes));
	            		/*
	            		System.out.println("Subiendo desde trabajadores diferentes mes: " + registro1.getMonth()+"\tNombre: "+registro1.getMonth()+"\tHoras: " +horasPorMes);
	            		System.out.println("Press Any Key To Continue...");
	                    new java.util.Scanner(System.in).nextLine();
	            		*/
	            		registro1 = setRegistro(rowActual);
	            		registrosMensualesId++;
	            		horasPorMes = 0;
	            		mes = registro2.getMonth();
	            	}else {
	            		/* Si los nombres coinciden pero la fecha no, actualizamos el registro base */
	            		if(!checkDate(registro1.getDate(), registro2.getDate())) {
	            			registro1 = setRegistro(rowActual);
	            			mes = registro2.getMonth();
	            		}else {
	            			//En este punto, el nombre y la fecha coinciden. Ahora corroboramos si trabajó al menos 1 hora
	            			System.out.println(" Buena jornada de Trabajo, campeón!");
	            			
	            			//Si las jornadas ya no tienen el mismo mes, 
	            			//se prosigue a subir lo que se consiguió del mes anterior
	            			if(registro1.getMonth() != mes) {
	            				jornadaService.save(new JornadaMensual(
		        						registrosMensualesId, 
		        						//Usamos el registro1 porque el 2 tiene la información del nuevo trabajador
		        						registroBackup.getName(), 
		        						mes, 
		        						horasPorMes));
	            				
	            	                    horasPorMes = 0;
	            			}
	            			registrosMensualesId++;
	            			
	            			horasPorMes += getHorasPorRegistro(registro1.getDate(), registro2.getDate());
	            			System.out.print("Horas acumuladas en el mes de "+ registro2.getMonth()+ " -> " 
	            						+String.format("%.2f", horasPorMes));
	            			numRegistros++;
	            			//Hacemos un backup, por si el siguiente registro tiene un mes diferente
	            			//y debemos recuperarlo
	            			registroBackup = registro1;
	            			registro1 = null; 
	            			mes = registro2.getMonth();
	            			
	            		} //Fin del if para comparar fechas
	            	} //Fin del if para comparar nombres
	                System.out.println();
	            }//Fin de ciclo while que recorre los datos del excel
	            jornadaService.save(new JornadaMensual(
						registrosMensualesId, 
						//Usamos el registro1 porque el 2 tiene la información del nuevo trabajador
						registroBackup.getName(), 
						mes, 
						horasPorMes));

	                    horasPorMes = 0;
	            System.out.println("Fin de la lectura del archivo");
	            System.out.println("Numero de Registros -> " + numRegistros);
	            file.close();
	            
	            response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
	            response.setHeader("Location", "http://localhost:8080/finalproject/");
	            response.setHeader("Connection", "close");
        } 
        catch (Exception e) { e.printStackTrace(); }
    
	}
	private boolean checkDate(Date dateInParsed, Date dateOutParsed) {
		//Checamos que el día coincida, al igual que el año
		boolean sameDay = validateMismoDia(dateInParsed, dateOutParsed);
		if(sameDay == false) {
			System.out.println("Días diferentes, cancelando ingreso...");
			return false;
		}
		
		//Checamos que al menos haya trabajado 1 hora para evitar 2 registros de entrada o 2 de salida.		
		double hours = getHorasPorRegistro(dateInParsed, dateOutParsed);
		if(hours < 1) {
			System.out.println("Registro con doble dato de entrada o salida, cancelando ingreso...");
			return false;
		}
		return true;
	}
	
	private boolean validateMismoDia(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		//Checamos que el día coincida, al igual que el año
		return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) 
				&& cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
	}
	
	private double getHorasPorRegistro(Date date1, Date date2) {
		DateTime dateTimeIn = new DateTime(date1);
		DateTime dateTimeOut = new DateTime(date2);
		Period p = new Period (dateTimeIn, dateTimeOut);
		
		//System.out.println("Horas -> " + p.getHours() + "\tMinutos -> " + p.getMinutes() + "\tSegundos = " + p.getSeconds());
		//Hacer esto parsea los datos para almacenarlos en forma de un entero.
		return (double)p.getHours() + ((double)p.getMinutes() / 60) + ((double)p.getSeconds()/ 3600);
		
	}
	
	public static Registro setRegistro(Row row) throws Exception {
		String nameIn = row.getCell(2).getStringCellValue();
        String dateIn = row.getCell(4).getStringCellValue();
        Date dateInParsed = StringToDate(dateIn);
		return new Registro(nameIn, dateInParsed);
	}
	
	public static Date StringToDate(String dateIn) throws Exception{
		//Partimos la fecha base en 2, la fecha y la hora
        String [] baseDateInParts = dateIn.split(" ");
        //Partimos la fecha en sus componentes; día, mes y año
        String [] dateInPartDay = baseDateInParts[0].split("-");
		//Juntamos los datos y retornamos como date
        String s = dateInPartDay[2]+"/"+ dateInPartDay[1]+"/"+ dateInPartDay[0]+" "+ baseDateInParts[1];
        System.out.println(s);
	    return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(s);  
	}
}
