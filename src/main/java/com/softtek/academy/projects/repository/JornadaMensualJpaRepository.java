package com.softtek.academy.projects.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.softtek.academy.projects.model.JornadaMensual;

public interface JornadaMensualJpaRepository extends JpaRepository<JornadaMensual, Long>{
	
	JornadaMensual findFirstByUsernameLike(String username);
	JornadaMensual findByUsernameAndMonth(String username, int month);
	List<JornadaMensual> findByMonth(int month);
}
