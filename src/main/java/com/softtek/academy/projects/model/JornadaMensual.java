package com.softtek.academy.projects.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class JornadaMensual {
	
	@Id
	private int jornadaId;

	private String username;
	private int month;
	private double hoursWorked;
	
	public JornadaMensual() {}
	
	public JornadaMensual(int jornadaId, String username, int month, double hoursWorked) {
		this.jornadaId = jornadaId;
		this.username = username;
		this.month = month;
		this.hoursWorked = hoursWorked;
	}
	
	public int getJornadaId() {return jornadaId;}
	public void setJornadaId(int jornadaId) {this.jornadaId = jornadaId;}
	
	public String getUsername() {return username;}
	public void setUsername(String username) {this.username = username;}
	
	public int getMonth() {return month;}
	public void setMonth(int month) {this.month = month;}
	
	public double getHoursWorked() {return hoursWorked;}
	public void setHoursWorked(double hoursWorked) {this.hoursWorked = hoursWorked;}
	
	
	@Override
	public String toString() {
		return "JornadaMensual "
				+ "[id= " + jornadaId
				+ ", user= " + username 
				+ ", month= " + month 
				+ ", hoursWorked= " + hoursWorked + "]";
	}
}
