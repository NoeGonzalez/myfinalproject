package com.softtek.academy.projects.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RegistroInOut {
	@Id
	private int id;
	private String name;
	private Date dateIn;
	private Date dateOut;
	
	public RegistroInOut() {}
	
	public RegistroInOut(int id, String name, Date dateIn, Date dateOut) {
		this.id = id;
		this.name = name;
		this.dateIn = dateIn;
		this.dateOut = dateOut;
	}
	
	public int getId() {return id;}
	public void setId(int id) {this.id = id;}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public Date getDate() {return dateIn;}
	public void setDate(Date dateIn) {this.dateIn = dateIn;}
	
	public Date getDateOut() {return dateOut;}
	public void setDateOut(Date dateOut) {this.dateOut = dateOut;}
	
	@Override
	public String toString() {
		return "ID: "+id+"\tName: " + name + "\tDateIn: " + dateIn +"\tDateOut: " + dateOut;
	}
	public int getMonthDateIn() {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(dateIn);
		return cal1.get(Calendar.MONTH) + 1;
	}
	
	public int getMonthDateOut() {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(dateOut);
		return cal1.get(Calendar.MONTH) + 1;
	}
}
