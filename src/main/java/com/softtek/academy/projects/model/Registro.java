package com.softtek.academy.projects.model;

import java.util.Calendar;
import java.util.Date;

public class Registro {
	private String name;
	private Date date;
	private Date dateOut;
	
	public Registro() {}
	
	public Registro(String name, Date date) {
		this.name = name;
		this.date = date;
	}
	public Registro(String name, Date date, Date dateOut) {
		this.name = name;
		this.date = date;
		this.dateOut = dateOut;
	}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public Date getDate() {return date;}
	public void setDate(Date date) {this.date = date;}
	
	public Date getDateOut() {return dateOut;}
	public void setDateOut(Date dateOut) {this.dateOut = dateOut;}
	
	@Override
	public String toString() {
		return "Name: " +  name + "\t Date: " + date;
	}
	public int getMonth() {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date);
		return cal1.get(Calendar.MONTH) + 1;
	}
}
