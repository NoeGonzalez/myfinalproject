package com.softtek.academy.projects.service;

import java.util.List;

import com.softtek.academy.projects.model.JornadaMensual;

public interface JornadaMensualService {
	JornadaMensual getByName(String name);
	
	void save(JornadaMensual jornada);
	void saveAll(List<JornadaMensual> list);
	
	JornadaMensual findByUsernameAndMonth(String username, int month);
	List<JornadaMensual> findByMonth(int month);
}
