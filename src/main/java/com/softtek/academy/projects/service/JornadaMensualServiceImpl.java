package com.softtek.academy.projects.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.projects.model.JornadaMensual;
import com.softtek.academy.projects.repository.JornadaMensualJpaRepository;

@Service
public class JornadaMensualServiceImpl implements JornadaMensualService{

	@Autowired
	private JornadaMensualJpaRepository jornadaJpaRepo;
	
	@Override
	public JornadaMensual getByName(String name) {
		return jornadaJpaRepo.findFirstByUsernameLike(name);
	}

	@Override
	public void save(JornadaMensual jornada) {
		jornadaJpaRepo.saveAndFlush(jornada);
		return;
	}
	
	@Override
	public void saveAll(List<JornadaMensual> list) {
		jornadaJpaRepo.save(list);
	}

	@Override
	public JornadaMensual findByUsernameAndMonth(String username, int month) {
		 return jornadaJpaRepo.findByUsernameAndMonth(username, month);
		
	}

	@Override
	public List<JornadaMensual> findByMonth(int month) {
		return jornadaJpaRepo.findByMonth(month);
	}

}
